<?php
function error_msg($msg){  
  ?>
  <div id="gayab" class="white error center text-center" onclick="hide()"><?php echo $msg ?></div>
  <?php
  
}

function success_msg($msg){	
  ?>
  <div id="gayab" class="white bg-alert center text-center" onclick="hide()"><?php echo $msg ?></div>
  <?php
  
}

function getDateBs(){
    include 'date/nepali-date.php';
	$nepali_date = new nepali_date();
	$year_en = date("Y",time());
	$month_en = date("m",time());
	$day_en = date("d",time());
	$date_ne = $nepali_date->get_nepali_date($year_en, $month_en, $day_en);
	$today_date=$date_ne['y'].'/'.$date_ne['m'].'/'.$date_ne['d'];
    return $today_date;
    
}


?>