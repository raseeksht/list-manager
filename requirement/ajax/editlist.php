<?php
include "../../db/db_connect.php";
$name = $_POST["name"];
$sql = "SELECT * FROM $name";
$result = $conn->query($sql);

?>
<table class="table">
    <thead>
        <td>sn</td>
        <td>date</td>
        <td>item</td>
        <td>price</td>
    </thead>
    <tbody>
        <?php
        $count = 0;
        $totalRow = $result->num_rows;
        while ($row = $result->fetch_assoc()){
            if ($totalRow == $row['id']){
                $addClass = "totalRow";
            }else{
                $addClass = '';
            }
            ?>
            <tr>
                <td class="<?php echo $addClass ?>"><?php echo $row['id'] ?></td>
                <td contenteditable="true" class="border date">
                    <?php echo $row['date'] ?>
                </td>
                <td contenteditable="true" class="border item">
                    <?php echo $row['item'] ?>
                </td>
                <td contenteditable="true" class="border price">
                    <?php echo $row['price'] ?>
                </td>
            </tr>
            
            <?php
            
            $count++;
        }
        ?>
    </tbody>
</table>
