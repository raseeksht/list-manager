
//monthly log loader
function showlog(name){
	$('.m-log-of').html("From <span class='pasale'>" + name+"</span>");
	$(".modal-footer").html("<button class='editList' onclick='editList(\""+name+"\")'>edit</button>");
	$.post('requirement/ajax/get-m-log.php',{
		name : name
	},function(data,status){
		$('.modal-body').html(data);
		
	});
}


function editList(name){
    $(".modal-footer").html("<button class='btn btn-primary saveBtn' onclick='editAndSave()'>Save</button>")
    $.post("requirement/ajax/editlist.php",
    {
        name:name
    },
    function(data){
        $(".modal-body").html(data);
        
    })
}


function editAndSave(){
	var totalRow = $(".totalRow").text();
	var name = $(".pasale").text();
	var i,id,tempPriceValue,tempDateValue,tempItemValue,priceList,dateList,itemList;
	var prices = [];
	var items = [];
	var dates = [];
	for (i=0;i<totalRow;i++){
		tempPriceValue = $(".price").eq(i).text();
		prices.push(tempPriceValue.trim());

		tempDateValue = $(".date").eq(i).text();
		dates.push(tempDateValue.trim());

		tempItemValue = $(".item").eq(i).text();
		items.push(tempItemValue.trim());

	}

	priceList = prices.join("|");
	dateList = dates.join("|");
	itemList = items.join("|");
	
	$.post("requirement/ajax/editAndSave.php",
	{
		priceList : priceList,
		itemList : itemList,
		dateList : dateList,
		name: name,
		totalRow:totalRow
	},
	function(data){
		// $(".ajax").html(data);
		alert(data)
	});
}